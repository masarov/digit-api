import sys
import math

MIN = sys.float_info.min
MAX = sys.float_info.max

class Point:
    def __init__(self, x, y, force, aziang, altang, time):
        self.x = x
        self.y = y
        self.force = force
        self.aziang = aziang
        self.altang = altang
        self.time = time

    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)

    def __repr__(self):
        return self.__str__()

class Stroke:
    def __init__(self):
        self.points = []
        self.order = -1
        self.max_x = MIN
        self.min_x = MAX
        self.max_y = MIN
        self.min_y = MAX
        self.area = 0.0
        self.length = 0
        # Add more parameters/features

    def __str__(self):
        return "({0}, {1}, {2})".format(self.order, len(self.points), self.area)

    def __repr__(self):
        return self.__str__()

    def add_point(self, point):
        self.points.append(point)

    #For plotting
    def get_coordinates(self, invertY = True):
        x_coords = []
        y_coords = []
        for point in self.points:
            x_coords.append(point.x)
            y_coords.append(-point.y if invertY else point.y)

        return x_coords, y_coords

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4)

class Drawing:
    def __init__(self):
        self.strokes = []
        # Other parameters...

    def __str__(self):
        return "({0})".format(len(self.strokes))

    def __repr__(self):
        return self.__str__()

    def add_stroke(self, stroke):
        self.strokes.append(stroke)

    def get_stroke(self, order):
        stroke_by_index = self.strokes[order]
        if stroke_by_index.order == order:
            return stroke_by_index
        else:
            print "Order ({0}) is not the same as index ({1})!".format(order, stroke_by_index.order)
            for stroke in self.strokes:
                if stroke.order == order:
                    return stroke
            # throw exception?

    def get_borders(self):
        max_x = MIN
        max_x_stroke = -1
        min_x = MAX
        min_x_stroke = -1
        max_y = MIN
        max_y_stroke = -1
        min_y = MAX
        min_y_stroke = -1

        for stroke in self.strokes:
            if stroke.max_x > max_x:
                max_x = stroke.max_x
                max_x_stroke = stroke.order

            if stroke.max_y > max_y:
                max_y = stroke.max_y
                max_y_stroke = stroke.order

            if stroke.min_x < min_x:
                min_x = stroke.min_x
                min_x_stroke = stroke.order

            if stroke.min_y < min_y:
                min_y = stroke.min_y
                min_y_stroke = stroke.order

        return max_x, max_x_stroke, min_x,min_x_stroke, max_y, max_y_stroke, min_y, min_y_stroke


class Helper:

    @staticmethod
    def euclidean_distance(p, q):
        return math.sqrt((q.x - p.x)**2 + (q.y - p.y)**2)

    @staticmethod
    def quadrant(center, p):
        if p.x >= center.x and p.y > center.y:
            return 1

        if p.x > center.x and p.y <= center.y:
            return 2

        if p.x <= center.x and p.y < center.y:
            return 3

        if p.x < center.x and p.y >= center.y:
            return 4

    @staticmethod
    def direction(full_direction):
        #todo: maybe regexp?
        if full_direction in "1234123412341234123412341234":
            return "CCW"

        if full_direction in "4321432143214321432143214321":
            return "CW"

        return "UNKNOWN" # ???

import numpy as np
from core import Drawing, Stroke, Point, Helper

def generate_drawing(json_data):
    json_strokes = []
    for i in range(0, len(json_data["data"])):
        json_strokes.append(json_data["data"][i])

    drawing = Drawing()

    for index, json_stroke in enumerate(json_strokes):

        stroke = Stroke()

        for json_point in json_stroke:
            if json_point["x"] > stroke.max_x:
                stroke.max_x = json_point["x"]
            if json_point["x"] < stroke.min_x:
                stroke.min_x = json_point["x"]
            if json_point["y"] > stroke.max_y:
                stroke.max_y = json_point["y"]
            if json_point["y"] < stroke.min_y:
                stroke.min_y = json_point["y"]

            stroke.add_point(Point(json_point["x"], json_point["y"], json_point["force"], json_point["aziang"], json_point["altang"], json_point["time"]))

        stroke.order = index
        stroke.width = stroke.max_x - stroke.min_x
        stroke.height = stroke.max_y - stroke.min_y
        stroke.area = stroke.width * stroke.height
        stroke.length = len(stroke.points)

        drawing.add_stroke(stroke)

    return drawing


def transform(stroke):
    x_coords, y_coords = stroke.get_coordinates()
    x_coords_shrink = []
    y_coords_shrink = []

    x_size = stroke.max_x - stroke.min_x
    y_size = stroke.max_y - stroke.min_y
    ratio = 20.0 / max(x_size, y_size)

    min_x = 1000000
    min_y = 1000000
    for point in stroke.points:
        new_x = -point.x * ratio
        new_y = point.y * ratio

        if new_x < min_x:
            min_x = new_x

        if new_y < min_y:
            min_y = new_y

        x_coords_shrink.append(new_x)
        y_coords_shrink.append(new_y)

    x_add_coords = []
    y_add_coords = []

    x_coords_len = len(x_coords_shrink)

    for i, val in enumerate(x_coords_shrink):
        x_coords_shrink[i] = int(round(x_coords_shrink[i] - min_x))
        y_coords_shrink[i] = int(round(y_coords_shrink[i] - min_y))

        if i > 0 and i < x_coords_len-1:
            prev_x = x_coords_shrink[i-1]
            prev_y = y_coords_shrink[i-1]

            curr_x = x_coords_shrink[i]
            curr_y = y_coords_shrink[i]

            diff_x = curr_x - prev_x
            diff_y = curr_y - prev_y

            small_x = min(curr_x, prev_x)
            small_y = min(curr_y, prev_y)

            diff_y_is_negative = diff_y < 0
            diff_x_is_negative = diff_x < 0

            if diff_x == 0 and diff_y == 0:
                continue

            y_is_smaller = abs(diff_y) < abs(diff_x)
            if y_is_smaller:
                last_y = prev_y
                for i in range(1, abs(diff_x)):
                    if i <= abs(diff_y):
                        last_y = prev_y + (-i if diff_y_is_negative else i)
                    x_add_coords.append(prev_x + (-i if diff_x_is_negative else i))
                    y_add_coords.append(last_y)
            else:
                last_x = prev_x
                for i in range(1, abs(diff_y)):
                    if i <= abs(diff_x):
                        last_x = prev_x + (-i if diff_x_is_negative else i)
                    x_add_coords.append(last_x)
                    y_add_coords.append(prev_y + (-i if diff_y_is_negative else i))

    number_matrix = np.zeros((28, 28))
    for i, val in enumerate(x_add_coords):
        number_matrix[y_add_coords[i] + 3][20 - x_add_coords[i]] = 1

    for i, val in enumerate(x_coords_shrink):
        number_matrix[y_coords_shrink[i] + 3][20 - x_coords_shrink[i]] = 1

    row_dict = {}

    # Make number_matrix more "Thick"
    for num, row in enumerate(number_matrix):
        if 1 in row:
            for index, item in enumerate(row):
                if item == 1:
                    if num not in row_dict:
                        row_dict[num] = []
                    row_dict[num].append(index-1)
                    row_dict[num].append(index+1)

    for key in row_dict.keys():
        for index in row_dict[key]:
            if index > -1 and index < 28:
                number_matrix[key][index] = 1

    return number_matrix

def find_circle(drawing):
    probability = 0

    # Find a stroke with max Area and max Length (+50% probability)
    max_area = -1
    stroke_with_max_area = -1
    max_length = -1
    stroke_with_max_length = -1

    for stroke in drawing.strokes:
        if stroke.area > max_area:
            max_area = stroke.area
            stroke_with_max_area = stroke.order

        if stroke.length > max_length:
            max_length = stroke.length
            stroke_with_max_length = stroke.order

    if stroke_with_max_area == stroke_with_max_length:
        probability = probability + 0.5

    # If the stroke is drawn first, it gives us + 10% probability
    if stroke_with_max_area == 0:
        probability = probability + 0.1

    # If the stroke is outer-border + 20% probability
    max_x, max_x_stroke, min_x, min_x_stroke, max_y, max_y_stroke, min_y, min_y_stroke = drawing.get_borders()
    if max_x_stroke == min_x_stroke and min_x_stroke == max_y_stroke and min_x_stroke == min_y_stroke:
        if max_x_stroke == stroke_with_max_area:
            probability = probability + 0.2

    return drawing.get_stroke(stroke_with_max_area)


def get_circle_params(stroke):
    circle_width = (stroke.max_x - stroke.min_x)
    circle_height = (stroke.max_y - stroke.min_y)

    x_center = (circle_width / 2) + stroke.min_x
    y_center = (circle_height / 2) + stroke.min_y

    sum_of_radiuses = 0
    center_point = Point(x_center, y_center, None, None, None, None)
    for point in stroke.points:
        sum_of_radiuses = sum_of_radiuses + abs(Helper.euclidean_distance(center_point, point))
    center_radius = sum_of_radiuses / len(stroke.points)

    # center_diameter = center_radius * 2
    # center_circumference = center_diameter * np.pi
    # center_area = np.pi * (center_radius**2)
    circle_dict = {}
    center_dict = {}
    center_dict["x"] = x_center
    center_dict["y"] = y_center
    circle_dict["center"] = center_dict
    circle_dict["radius"] = center_radius

    return circle_dict

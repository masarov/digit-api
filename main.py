import tensorflow as tf
import numpy as np
import sys
sys.path.append('mnist')
import model
from core import Drawing, Stroke
import processing

# Restore the saved model
x = tf.placeholder("float", [None, 784])
sess = tf.Session()

with tf.variable_scope("convolutional"):
    keep_prob = tf.placeholder("float")
    y2, variables = model.convolutional(x, keep_prob)

saver = tf.train.Saver(variables)
saver.restore(sess, "mnist/data/convolutional.ckpt")

def convolutional(input):
    return sess.run(y2, feed_dict={x: input, keep_prob: 1.0}).flatten().tolist()


# Web application
from flask import Flask, jsonify, render_template, request

app = Flask(__name__)
app.config['DEBUG'] = True #TODO: REMOVE!

if __name__ == '__main__':
    handler = RotatingFileHandler('foo.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    app.run()

@app.route('/')
def index():
    app.logger.warning('A warning occurred (%d apples)', 42)
    app.logger.error('An error occurred')
    app.logger.info('Info')
    return "It works!"

@app.route('/api/test', methods=['POST'])
def test():
    app.logger.info(request.json)
    return "great"

@app.route('/api/digit', methods=['POST'])
def mnist():
    app.logger.info(request.content_length)
    app.logger.info("Started processing drawing..")
    app.logger.info(request.json)
    drawing = processing.generate_drawing(request.json)
    results = []

    circle = processing.find_circle(drawing)
    circle_params = processing.get_circle_params(circle)

    for stroke in drawing.strokes:
        max_probability = 0
        digit = -1
        digit_dict = {}

        if(stroke.order == circle.order):
            continue
        result = convolutional(np.array(processing.transform(stroke), dtype=np.uint8).reshape(1, 784))

        for index, probability in enumerate(result):
            if(probability > max_probability):
                max_probability = probability
                digit = index

        digit_dict["digit"] = digit
        digit_dict["probability"] = max_probability

        max_dict = {}
        max_dict["x"] = stroke.max_x
        max_dict["y"] = stroke.max_y

        min_dict = {}
        min_dict["x"] = stroke.min_x
        min_dict["y"] = stroke.min_y

        digit_dict["max"] = max_dict
        digit_dict["min"] = min_dict

        results.append(digit_dict)

    return jsonify(digits=results, circle=circle_params)
